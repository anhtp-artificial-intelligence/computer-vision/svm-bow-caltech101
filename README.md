# For learning computer vision.
Just simple implement of bag of word, knn, svm algorithms on caltech_101 dataset

# Requirement:
Python 3.6, scikit-learn, numpy, cv2 (opencv-dev), scipy, matplotlib